import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 as uuidv4 } from "uuid";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";

//  <--- components ---->
import TodoItem from "./components/todoItem/TodoItem";
import TodoList from "./components/todoList/TodoList";

class App extends Component {
  state = {
    todos: todosList,
    completed: [],
    incomplete: [],
  };

  // <---- add todo ---->
  handleAddTodo = (evt) => {
    let newTodo2 = {};
    if (evt.key === "Enter") {
      newTodo2.todos = [...this.state.todos];
      newTodo2.todos.push({
        userId: 1,
        id: uuidv4(),
        title: evt.target.value,
        completed: false,
      });
      // const newTodos = this.state.todos.slice();
      // newTodos.push(newTodo);
      this.setState(newTodo2);
      evt.target.value = "";
      console.log(newTodo2);
    }
  };

  // <---- toggle complete ---->
  handleComplete = (id) => {
    let newTodos = this.state.todos.map((todo) => {
      if (todo.id === id) {
        return { ...todo, completed: !todo.completed };
      }
      return todo;
    });
    this.setState({ todos: newTodos });
  };

  // <---- delete a todo item ---->
  handleDelete = (todoItemId) => {
    const newTodos = this.state.todos.filter((todo) => todo.id !== todoItemId);
    console.log("delete");
    this.setState({ todos: newTodos });
  };

  // <---- Clear completed Todos ---->
  handleClearComplete = () => {
    let clearedTodos = this.state.todos.filter(
      (todo) => todo.completed === false
    );
    console.log("clear em out");
    this.setState({ todos: clearedTodos });
  };

  // <----- button functions --->

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            onKeyDown={this.handleAddTodo}
          />
        </header>
        <Route
          exactClassName="is-selected"
          exact
          path="/"
          render={() => (
            <TodoList
              todos={this.state.todos}
              handleComplete={this.handleComplete}
              handleClearComplete={this.handleClearComplete}
              handleDelete={this.handleDelete}
            />
          )}
        />
        <Route
          exactClassName="is-selected"
          exact
          path="/active"
          render={() => (
            <TodoList
              todos={this.state.todos.filter(
                (todo) => todo.completed === false
              )}
              handleComplete={this.handleComplete}
              handleClearComplete={this.handleClearComplete}
              handleDelete={this.handleDelete}
            />
          )}
        />
        <Route
          exactClassName="is-active"
          exact
          path="/completed"
          render={() => (
            <TodoList
              todos={this.state.todos.filter((todo) => todo.completed === true)}
              handleComplete={this.handleComplete}
              handleClearComplete={this.handleClearComplete}
              handleDelete={this.handleDelete}
            />
          )}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>
              {" "}
              {
                this.state.todos.filter((todo) => todo.completed === false)
                  .length
              }
            </strong>{" "}
            item(s) left
          </span>
          <ul className="filters">
            <nav
              activeKey="/home"
              onSelect={(selectedKey) => alert(`selected ${selectedKey}`)}
            >
              <li>
              <NavLink
                  to="/"
                  exact={true}
                  activeStyle={{
                    background: "red",
                    color: "white",
                  }}
                >
                  All
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/active"
                  activeStyle={{
                    background: "red",
                    color: "white",
                  }}
                >
                  Active
                </NavLink>
                {/* <NavLink to="/active" id="active">Active</NavLink> */}
              </li>
              <li>
              <NavLink
                  to="/Completed"
                  activeStyle={{
                    background: "red",
                    color: "white",
                  }}
                >
                  Completed
                </NavLink>
                {/* <NavLink to="/completed" id="complete">
                  Completed
                </NavLink> */}
              </li>
            </nav>
          </ul>

          <button
            className="clear-completed"
            onClick={this.handleClearComplete}
          >
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

export default App;
