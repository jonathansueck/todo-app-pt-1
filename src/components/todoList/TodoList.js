import React, { Component }  from 'react'
import TodoItem from "../todoItem/TodoItem"

class TodoList extends Component {
    render() {
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map((todo) => (
              <TodoItem
                key={todo.id}
                title={todo.title}
                completed={todo.completed}
                handleDelete={(evt) => this.props.handleDelete(todo.id)}
                handleComplete={(evt) => this.props.handleComplete(todo.id)}
              />
            ))}
          </ul>
        </section>
      );
    }
  }

export default TodoList